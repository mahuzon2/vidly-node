require("./src/logger");
require("express-async-errors");
require("./src/db")();
require("./src/startup/config")();
const express = require("express");
const routes = require("./startup/routes");

const app = express();
routes(app);

const port = process.env.PORT || 3000;

app.listen(port, () => console.log(`listening on port ${port}...`));
