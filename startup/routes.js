const express = require("express");
const genres = require("../src/routes/genres");
const customers = require("../src/routes/customers");
const movies = require("../src/routes/movies");
const rentals = require("../src/routes/rentals");
const users = require("../src/routes/users");
const auth = require("../src/routes/auth");
const error = require("../src/middleware/error");

modules.exports = function (app) {
  app.use(express.json());
  app.use(express.static("."));

  app.use("/api/genres", genres);
  app.use("/api/customers", customers);
  app.use("/api/movies", movies);
  app.use("/api/rentals", rentals);
  app.use("/api/users", users);
  app.use("/api/auth", auth);
  app.use(error);
};
