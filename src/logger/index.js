require("winston-mongodb");
const { createLogger, transports, format } = require("winston");
require("winston-daily-rotate-file");

const logFormat = format.printf(
  ({ timestamp, level, message }) => `${timestamp} ${level}: ${message}`
);
module.exports = createLogger({
  format: format.combine(
    format.timestamp({
      format: "YYYY-MM-DD HH:mm:ss",
    }),
    format.colorize(),
    format.json(),
    logFormat
  ),
  transports: [
    new transports.DailyRotateFile({
      filename: "./logs/%DATE%/combine.log",
      datePattern: "YYYY-MM-DD-HH",
      zippedArchive: true,
      maxSize: "20m",
      maxFiles: "14d",
    }),
    new transports.DailyRotateFile({
      filename: "./logs/%DATE%/error.log",
      datePattern: "YYYY-MM-DD-HH",
      zippedArchive: true,
      maxSize: "20m",
      maxFiles: "14d",
      level: "error",
    }),
    new transports.DailyRotateFile({
      filename: "./logs/%DATE%/info.log",
      datePattern: "YYYY-MM-DD-HH",
      zippedArchive: true,
      maxSize: "20m",
      maxFiles: "14d",
      level: "info",
    }),
    new transports.Console(),
    new transports.MongoDB({
      db: "mongodb://localhost:27017/vidly-logs",
    }),
  ],
  exceptionHandlers: [
    new transports.Console({ colorize: true, prettyPrint: true }),
    new transports.DailyRotateFile({
      filename: "./logs/%DATE%/exceptions.log",
      datePattern: "YYYY-MM-DD-HH",
      zippedArchive: true,
      maxSize: "20m",
      maxFiles: "14d",
    }),
  ],
  rejectionHandlers: [
    new transports.Console({ colorize: true, prettyPrint: true }),
    new transports.DailyRotateFile({
      filename: "./logs/%DATE%/rejections.log",
      datePattern: "YYYY-MM-DD-HH",
      zippedArchive: true,
      maxSize: "20m",
      maxFiles: "14d",
    }),
  ],
});
