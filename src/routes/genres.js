const { Router } = require("express");
const { Genre, validate } = require("../models/Genre");
const router = Router();

router.get("/", async (req, res) => {
  throw new Error("Not implemented");
  const genres = await Genre.find();
  res.send(genres);
});

router.get("/:id", async (req, res) => {
  const id = req.params.id;

  try {
    const genre = await Genre.findById(id);
    res.send(genre);
  } catch (e) {
    res.status(404).send("Not Found  " + e.message);
  }
});

router.post("/", async (req, res) => {
  //valider
  const { error } = validate(req.body);

  if (error) return res.status(400).send(error);

  const genre = new Genre({
    // id: genres.length + 1,
    ...req.body,
  });

  //   genres.push(genre);
  try {
    let result = await genre.save();
    res.status(201).send(result);
  } catch (e) {
    res.status(404).send(e.message);
  }
});

router.put("/:id", async (req, res) => {
  const id = req.params.id;

  let genre;

  try {
    genre = await Genre.findById(id);
  } catch (e) {
    res.status(404).send(new Error("Not Found"));
  }

  const { error } = validate(req.body);

  if (error) return res.status(400).send(error);

  let { name, description } = req.body;
  console.log({ name, description });
  try {
    let result = await genre.updateOne(
      { name: name, description },
      { new: true }
    );
    res.status(200).send(result);
  } catch (e) {
    res.status(404).send(`FAILED_TO_UPDATE => ${e.message}`);
  }
});

router.delete("/:id", async (req, res) => {
  const id = req.params.id;

  try {
    let genre = await Genre.findByIdAndDelete(id);
    res.status(200).send(genre);
  } catch (e) {
    res.status(404).send("Not Found => " + e.message);
  }
});

module.exports = router;
