const { Router, response } = require("express");
const Joi = require("joi");
const { Customer, validate } = require("../models/Customer");

const route = Router();

route.get("/", async (req, res) => {
  const customer = await Customer.find();
  res.status(200).send(customer);
});

route.get("/:id", async (req, res) => {
  const id = req.params.id;
  try {
    let customer = await Customer.findById(id);
    if (!customer)
      return res
        .status(404)
        .send(`Customer with id ${req.params.id} is not found`);
    res.send(result);
    res.status(200).send(customer);
  } catch (e) {
    res.status(404).send(`ERROR_FINDING_CUSTOMER ==> ${e.message}`);
  }
});

route.post("/", async (req, res) => {
  const error = validate(req.body);
  if (error) return res.status(403).send("SCHEMA_MISMATCH ==>" + error.message);
  try {
    const customer = new Customer({
      ...req.body,
    });
    let result = await customer.save();
    res.status(201).send(result);
  } catch (e) {
    res.status(403).send(`FAILLED_TO_CREATE_CUSTOMER ==> ${e.message}`);
  }
});

route.put("/:id", function (req, res) {
  const error = validate(req.body);
  if (error) return res.status(403).send("SCHEMA_MISMATCH ==>" + error.message);

  try {
    let customer = Customer.findByIdAndUpdate(
      req.params.id,
      { ...req.body },
      { new: true }
    );
    res.status(200).send(customer);
  } catch (e) {
    res.status(403).send(`FAILLED_TO_UPDATE_CUSTOMER ==> ${e.message}`);
  }
});

route.delete("/:id", async (req, res) => {
  try {
    let result = await Customer.findByIdAndRemove(req.params.id);
    if (!result)
      return res
        .status(404)
        .send(`Customer with id ${req.params.id} is not found`);
    res.send(result);
  } catch (e) {
    res.send(`FAILED_TO_DELETER ==> ${e.message}`);
  }
});

module.exports = route;
