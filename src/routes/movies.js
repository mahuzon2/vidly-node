const { Router } = require("express");
const { Movie, validate } = require("../models/Movie");
const { Genre } = require("../models/Genre");
const router = Router();

router.get("/", async (req, res) => {
  const movies = await Movie.find();
  res.send(movies);
});

router.get("/:id", async (req, res) => {
  const id = req.params.id;

  try {
    const movie = await Movie.findById(id);
    res.send(movie);
  } catch (e) {
    res.status(404).send("Not Found  " + e.message);
  }
});

router.post("/", async (req, res) => {
  //valider
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error);

  let genreId = req.body.genreId;
  let genre;
  try {
    genre = await Genre.findById(genreId);
  } catch (error) {
    return res.status(400).send(`SOMETING_WENT_WRONG ==> ${error.message}`);
  }
  if (!genre)
    return res.status(404).send(`genre with id ${genreId} is not found`);

  const movie = new Movie({
    title: req.body.title,
    genre: {
      _id: genre.id,
      name: genre.name,
      //   description: req.body.genre.description,
    },
    numberInStock: req.body.numberInStock,
    dailyRentalRate: req.body.dailyRentalRate,
  });

  //   genres.push(genre);
  try {
    let result = await movie.save();
    res.status(201).send(result);
  } catch (e) {
    res.status(404).send(e.message);
  }
});

router.put("/:id", async (req, res) => {
  const id = req.params.id;
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error);

  let genreId = req.body.genreId;
  let genre;
  try {
    genre = await Genre.findById(genreId);
  } catch (error) {
    return res.status(400).send(`SOMETING_WENT_WRONG ==> ${error.message}`);
  }

  let { title } = req.body;
  try {
    let result = await Movie.findByIdAndUpdate(
      id,
      {
        title: title,
        genre: {
          _id: genreId,
          name: genre.name,
        },
      },
      { new: true }
    );
    res.status(200).send(result);
  } catch (e) {
    res.status(404).send(`FAILED_TO_UPDATE => ${e.message}`);
  }
});

router.delete("/:id", async (req, res) => {
  const id = req.params.id;

  try {
    let genre = await Movie.findByIdAndDelete(id);
    res.status(200).send(genre);
  } catch (e) {
    res.status(404).send("Not Found => " + e.message);
  }
});

module.exports = router;
