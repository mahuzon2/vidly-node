const { Router } = require("express");
const { User, validate } = require("../models/User");
const bcrypt = require("bcrypt");
const _ = require("underscore");
const auth = require("../middleware/auth");
const router = Router();

router.post("/", async (req, res) => {
  const reqUser = req.body;
  const { error } = validate(reqUser);
  if (error) return res.status(400).send(error.details[0].message);

  // let user;
  let user = await User.findOne({ email: reqUser.email });

  if (user) return res.status(400).send("User_already_exists");
  //   const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(req.body.password, 10);

  user = new User({
    name: reqUser.name,
    email: reqUser.email,
    password: hashedPassword,
    isAdmin: reqUser.isAdmin || false,
  });

  try {
    await user.save();
  } catch (err) {
    return res.status(400).send(`an error occurred ==> ${err.message}`);
  }
  const token = user.generateToken();
  let response = _.omit(user, ["name"]);
  res.header("x-auth-token", token).send(response);
});

router.get("/", auth, async (req, res) => {
  let user = await User.findById(req.user._id);
  res.send(_.omit(user, "password"));
});
module.exports = router;
