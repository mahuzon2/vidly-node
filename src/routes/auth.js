const { Router } = require("express");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const Joi = require("joi");
const { User } = require("../models/User");

const router = Router();

router.post("/", async (req, res) => {
  const { error } = validateBody(req.body);

  if (error) return res.status(400).send(error.details[0].message);
  const user = await User.findOne({ email: req.body.email });

  if (!user) return res.status(400).send("invalid email or password");


  const isValidPassword = await bcrypt.compare(
    req.body.password,
    user.password
  );

  if (!isValidPassword)
    return res.status(400).send("invalid email or password");

  const token = user.generateToken();

  res.send(token);
});

const validateBody = (body) => {
  const schema = {
    email: Joi.string().required().email(),
    password: Joi.string().required(),
  };

  return Joi.validate(body, schema);
};
module.exports = router;
