const { Router } = require("express");
const { validate, Rental } = require("../models/Rental");
const { Customer } = require("../models/Customer");
const { Movie } = require("../models/Movie");

const router = Router();

router.get("/", async (req, res) => {
  const rentals = await Rental.find().sort("-dateOut");

  res.send(rentals);
});

router.get("/:id", async (req, res) => {
  let id = req.params.id;

  let rental;
  try {
    rental = await Rental.findById(id);
  } catch (error) {
    return res.status(400).send(`bad rental id : ${id}`);
  }

  if (!rental) return res.status(404).send(`rental not found`);

  res.send(rental);
});

router.post("/", async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const customerId = req.body.customerId;
  const movieId = req.body.movieId;

  let movie;
  try {
    movie = await Movie.findById(movieId);
  } catch (error) {
    return res.status(400).send(`bad movie id : ${movieId}`);
  }
  if (!movie) return res.status(404).send(`movie not found`);
  if (movie.numberInStock <= 0)
    return res.status(404).send(`movie not instock`);

  let customer;
  try {
    customer = await Customer.findById(customerId);
  } catch (error) {
    return res.status(400).send(`bad customer id : ${customerId}`);
  }
  if (!customer) return res.status(404).send(`customer not found`);

  let rental = new Rental({
    customer: {
      _id: customer.id,
      name: customer.name,
      isGold: customer.isGold,
      phone: customer.phone,
    },
    movie: {
      _id: movie.id,
      title: movie.title,
    },
  });
  await rental.save();

  // decrementer le nombre de movies restants apres un pret

  movie.numberInStock--;
  movie.save();

  res.status(201).send(rental);
});

module.exports = router;
