const config = require("config");
const jwt = require("jsonwebtoken");

function auth(req, res, next) {
  const token = req.headers["x-auth-token"];
  if (!token) return res.status(401).send("missing x-auth-token");

  try {
    let decoded = jwt.decode(token, config.get("jwtScretKey"));
    req.user = decoded;
    next();
  } catch (err) {
    return res.status(403).send("invalid token");
  }
}

module.exports = auth;
