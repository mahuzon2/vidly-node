const { model, Schema } = require("mongoose");

const sch = new Schema({
  name: {
    type: String,
    minlength: 3,
    required: true,
  },
  phone: {
    type: Number,
    required: true,
  },
  isGold: Boolean,
});

const Customer = model("Customer", sch);

module.export = Customer;
