const { Schema, model } = require("mongoose");
const { GenreSchema } = require("./Genre");
const Joi = require("joi");

const Movie = model(
  "Movie",
  new Schema({
    title: {
      type: String,
      required: true,
      minlength: 3,
      maxlength: 50,
      unique: true,
    },
    genre: { type: GenreSchema, required: true },
    numberInStock: { type: Number, min: 0, default: 0 },
    dailyRentalRate: { type: Number, required: true, default: 0 },
  })
);

function validateMovie(movie) {
  const schema = Joi.object({
    title: Joi.string().required().min(3).max(50),
    genreId: Joi.string().required(),
    numberInStock: Joi.number().min(0).max(50),
    dailyRentalRate: Joi.number().min(0).max(50),
  });
  const result = schema.validate(movie);

  return result;
}

exports.Movie = Movie;
exports.validate = validateMovie;
