const { Schema, model } = require("mongoose");
const jwt = require("jsonwebtoken");
const config = require("config");
const Joi = require("joi");

const userSchema = new Schema({
  name: {
    type: "string",
    required: true,
    minlength: 5,
    maxlength: 255,
  },
  email: {
    type: "string",
    required: true,
    minlength: 5,
    maxlength: 255,
    unique: true,
    match: /.*@..*/,
  },
  password: {
    type: "string",
    required: true,
    minlength: 5,
    maxlength: 255,
  },
  isAdmin: {
    type: "boolean",
    default: false,
  },
});

userSchema.method({
  generateToken: function () {
    return jwt.sign(
      { _id: this._id, isAdmin: this.isAdmin },
      config.get("jwtScretKey")
    );
  },
});

exports.User = model("User", userSchema);

exports.validate = function (user) {
  const schema = Joi.object({
    name: Joi.string().required().min(5).max(255),
    email: Joi.string().required().min(5).max(255),
    password: Joi.string().required().min(5).max(255),
    isAdmin: Joi.boolean(),
  });

  return schema.validate(user);
};
