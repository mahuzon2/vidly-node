const Joi = require("joi");
const mongoose = require("mongoose");

const Rental = new mongoose.model(
  "Rental",
  new mongoose.Schema({
    customer: {
      type: new mongoose.Schema({
        name: {
          type: "string",
          required: true,
          minLength: 5,
          maxLength: 50,
        },
        isGold: {
          type: "boolean",
          default: false,
        },
        phone: {
          type: "number",
          required: true,
          minLength: 5,
          maxLength: 50,
        },
      }),
      required: true,
    },
    movie: {
      type: new mongoose.Schema({
        title: {
          type: "string",
          required: true,
          trim: true,
          minLength: 5,
          maxLength: 50,
        },
        dailyRentalRate: {
          type: "number",
          required: true,
          min: 0,
          max: 255,
        },
      }),
      required: true,
    },
    dateOut: {
      type: Date,
      required: true,
      default: Date.now,
    },
    dateReturned: Date,
    rentalFee: {
      type: "number",
      min: 0,
    },
  })
);

const validateRental = (rental) => {
  const schema = Joi.object({
    customerId: Joi.string().required(),
    movieId: Joi.string().required(),
  });

  return schema.validate(rental);
};

exports.Rental = Rental;
exports.validate = validateRental;
