const { model, Schema } = require("mongoose");

const Customer = model(
  "Customer",
  new Schema({
    name: {
      type: String,
      minlength: 3,
      required: true,
    },
    phone: {
      type: Number,
      required: true,
    },
    isGold: { type: Boolean, default: false },
  })
);

const validate = (customer) => {
  const schema = Joi.object({
    name: Joi.string().required().min(3),
    phone: Joi.number().required(),
    isGold: Joi.boolean(),
  });

  let { error } = schema.validate(customer);
  return error;
};
exports.Customer = Customer;
exports.validate = validate;
