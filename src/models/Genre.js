const { Schema, model } = require("mongoose");
const Joi = require("joi");
const genreSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  description: String,
});

const Gender = model("Genre", genreSchema);

function validateGenre(genre) {
  const schema = Joi.object({
    name: Joi.string().min(3).required(),
    description: Joi.string().min(6),
  });

  return schema.validate(genre);
}

exports.validate = validateGenre;
exports.Genre = Gender;
exports.GenreSchema = genreSchema;
